# README #

Simple BufferedReader and StringBuffer for swift 

## Use with path

```swift
let bufferedReader: BufferedReader? = BufferedReader(withPath: "/test.txt")
if let reader = bufferedReader{
   while reader.hasMoreContent(){
       if let line = reader.readLine(){
           print(line)
       }
   }
   reader.close()
}
else{
   print("Error")
}
```

##Use with InputStream

```swift
if let inputStream: InputStream = InputStream(fileAtPath: "/test.txt"){
   inputStream.open()
   let bufferedReader: BufferedReader = BufferedReader(withInputStream: inputStream)
   while bufferedReader.hasMoreContent(){
       if let line = bufferedReader.readLine(){
           print(line)
       }
   }
   inputStream.close()
}
```
## Use StringBuffer

```swift
var stringBuffer = StringBuffer(withCapacity: 500)
for i in 0...100{
    stringBuffer.append("This is a test string")
}

print(stringBuffer.toString())
```