import Foundation

// Example usage
//
// let bufferedReader: BufferedReader? = BufferedReader(withPath: "test.txt")
// if let reader = bufferedReader{
//    while reader.hasMoreContent(){
//        if let line = reader.readLine(){
//            print(line)
//        }
//    }
//    reader.close()
// }
// else{
//    print("Error..")
// }
//
// Another usage
//
//if let inputStream: InputStream = InputStream(fileAtPath: "test.txt"){
//    inputStream.open()
//    let bufferedReader: BufferedReader = BufferedReader(withInputStream: inputStream)
//    while bufferedReader.hasMoreContent(){
//        if let line = bufferedReader.readLine(){
//            print(line)
//        }
//    }
//    inputStream.close()
//}



public class BufferedReader {
    private var _inStream: InputStream?

    public init(withInputStream inStream: InputStream){
        self._inStream = inStream
    }

    public init?(withPath path: String){
        guard FileManager.default.fileExists(atPath: path) else{
            return nil
        }
        self._inStream = InputStream(fileAtPath: path)
        
        guard self._inStream != nil else {
            return nil
        }

        self._inStream!.open()
    }

    public func hasMoreContent() ->Bool{
        return (self._inStream!.hasBytesAvailable)
    }

    public func readLine() -> String? {
        var _arr: Array<UInt8> = [UInt8]()
        while (self._inStream!.hasBytesAvailable) {
            var _tmp: Array<UInt8> = [UInt8](repeating: 0, count: 1)
            let _ = self._inStream?.read(&_tmp, maxLength: 1)
            if _tmp[0] == 10 {
                return String(bytes: _arr, encoding: .utf8)
            }
            _arr.append(_tmp[0])

        }
        if _arr[0] == 0 {
            return nil
        }
        else {
            return String(bytes: _arr, encoding: .utf8)
        }
    }

    public func close(){
        self._inStream!.close()
    }
}
