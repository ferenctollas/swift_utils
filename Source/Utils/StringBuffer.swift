import Foundation
// Example usage:
// let startTime:CFAbsoluteTime = CFAbsoluteTimeGetCurrent()
// var endTime:CFAbsoluteTime?
// var stringBuffer = StringBuffer(withCapacity: 500)
//
// for i in 0...100{
//     stringBuffer.append(";asdf  asdfkashflkashflaskf laskf lkasdfh laks flkajsdf lakshfdj")
// }
//
// endTimeBuffer = CFAbsoluteTimeGetCurrent()
// print("End time: \(endTimeBuffer! - startTimeBuffer)")
// print(stringBuffer.toString())

public class StringBuffer {
    private var buffer: Data?

    public init(){
        self.buffer = Data(capacity: 500)
    }

    public init(withCapacity: UInt64){
        self.buffer = Data(capacity: Int(withCapacity))
    }

    public func append(_ s: String?){
        if s == nil{
            return
        }
        self.buffer?.append(s!.data(using: .utf8)!)
    }

    public func toString() -> String{
        return String(bytes: self.buffer!, encoding: .utf8)!
    }
}
